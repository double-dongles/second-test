/*

The database will be structs of computer items with the specified property in
a vector, which is while rudimentary and further improvement could be made.

However since this is only a high school coding problem, I do not expect the
requirement of optimization since pre-optimization is the root of all evil.

As an added challange, I will try to be using *functional programming*
(this prove to be a nice way of refreshing my brain as well as
reminding me how much better Haskell is) operations for the clean code.

Additionally, I will be minimalizing the amount of crates used except for
quality-of-life crates, because I'm lazy as hell

Sincerely yours,
Dang Nguyen
*/

use itertools::Itertools;
use std::fs::File;
use std::io::prelude::*;

//Define item
#[derive(Debug)]
struct Item {
    //Id is store as the vector key
    name: String,
    quantity: u32,

    //TODO: Use actual data type that designed for date
    date: u32,
    month: u32,
    year: u32,
}

impl Item {
    fn new(name: String, quantity: u32, date: u32, month: u32, year: u32) -> Item {
        Item {
            name,
            quantity,
            date,
            month,
            year,
        }
    }
}

fn main() {
    println!("Test number 2:\n");

    let mut items: Vec<(String, Item)> = vec![];

    //Requirement 1
    loop {
        let id: String = rprompt::prompt_reply("Id:").unwrap().trim().to_owned();
        if id.is_empty() {
            break;
        }
        let part: String = rprompt::prompt_reply("Part:").unwrap().trim().to_owned();
        let quantity: u32 = rprompt::prompt_reply("Quantity:").unwrap().parse().unwrap();
        let date: u32 = rprompt::prompt_reply("Date:").unwrap().parse().unwrap();
        let month: u32 = rprompt::prompt_reply("Month:").unwrap().parse().unwrap();
        let year: u32 = rprompt::prompt_reply("Year:").unwrap().parse().unwrap();
        items.push((id, Item::new(part, quantity, date, month, year)));
    }

    //Requirement 2
    //https://en.wikipedia.org/wiki/Fold_(higher-order_function)
    let amount = items
        .iter()
        .fold(0, |acc: u32, (_, item)| acc + item.quantity);
    println!("\nTotal amount of items in store: {amount}\n");

    //Requirement 3
    let sorted_by_quantity = items
        .iter()
        .sorted_by(|(a, _), (b, _)| a.cmp(b)) //Sort the items ascending via ID
        .group_by(|(id, _)| id) //Group by ID into subgroups
        .into_iter() //Safe to transfer ownership
        .map(|(_, xs)| xs.into_iter().max_by_key(|(_, x)| x.quantity).unwrap()) //Get the item with maximum quantity of each ID subgroup
        .collect_vec();
    for (_, item) in sorted_by_quantity {
        println!(
            "Name: {}, Quantity: {}, Date:{}/{}/{}",
            item.name, item.quantity, item.date, item.month, item.year
        );
    }

    //Requirement 4
    //https://en.wikipedia.org/wiki/Filter_(higher-order_function)
    let april2023 = items
        .iter()
        .filter(|(_, x)| x.month == 4 && x.year == 2023) //Filters out item that didn't arrive on April and 2023
        .collect_vec();
    print!("\nApril 2023 items: writing [");
    let mut file = File::create("Luutru.txt").unwrap();
    for (_, item) in april2023 {
        write!(
            file,
            "Name: {}, Quantity: {}, Date:{}/{}/{}\n",
            item.name, item.quantity, item.date, item.month, item.year,
        )
        .unwrap();
        print!("=");
    }
    println!("] Done");
}
